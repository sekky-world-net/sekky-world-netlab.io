{
  extras = hackage:
    {
      packages = {
        "slick" = (((hackage.slick)."1.0.1.0").revisions).default;
        sekky-world = ./sekky-world.nix;
        };
      };
  resolver = "lts-14.17";
  modules = [ ({ lib, ... }: { packages = {}; }) { packages = {}; } ];
  }