---
title: "Face Pulling - Rapid Ghetto Skeletal Expansion For Adults"
author: Sekky
date: Mar 5, 2020
tags: [fitness, posture]
description: Face Pulling
image: facepulling.jpg
---

You've discovered mewing. Maybe you've been trying it for a week. Your hyoid
has moved up and makes you look slightly less like a melted potato. You're
starting to get a little glimpse of what you might look like, maybe, in several
years.

You want faster results, and want to try pulling your skull back into it's
correct position with hard mechanical force.

Face pulling definitely can work to loosen up your sutures and make your mewing
more effective, but it's not a substitute for mewing. The important part is to
not overdo it, don't try to move too quickly, and most importantly don't try
and mix this with traditional orthodontics.
