# Sekky.World

[sekky.world](https://sekky.world) built with [slick](https://github.com/ChrisPenner/slick)!

## Updating Stack/Nix

```
stack-to-nix --output nix --stack-yaml stack.yaml
nix-prefetch-git --quiet https://github.com/NixOS/nixpkgs | tee nixpkgs-src.json
nix-prefetch-git --quiet https://github.com/input-output-hk/haskell.nix | tee haskell-nix-src.json
```


